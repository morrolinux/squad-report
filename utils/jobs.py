import os
from utils.projects import get_project
from utils.env_vars import ENV_JOBS


def parse_jobs(squad, args):
    jobs = []

    sources = (
        ("args", args.jobs),
        (
            ENV_JOBS,
            [item.strip() for item in os.environ.get(ENV_JOBS).split(";")]
            if os.environ.get(ENV_JOBS)
            else None,
        ),
    )

    for source_name, triplets in sources:
        if not source.strip():
            continue

        if len(triplets) == 1 and triplets[0] == "":
            continue

        group_slug = None
        group = None
        project_slug = None
        project = None
        build_version = None
        build = None

        for triplet in triplets:
            fq_builds = [item.strip() for item in triplet.split(",")]
            for fq_build in fq_builds:
                parts = [item.strip() for item in fq_build.split(":")]
                # the only item means build
                if len(parts) == 1:
                    if parts[0]:
                        build_version = parts[0]
                        build = None
                # two items mean project and build
                if len(parts) == 2:
                    if parts[0]:
                        project_slug = parts[0]
                        project = None
                    if parts[1]:
                        build_version = parts[1]
                        build = None
                # all three are present
                if len(parts) >= 3:
                    if parts[0]:
                        group_slug = parts[0]
                        group = None
                    if parts[1]:
                        project_slug = parts[1]
                        project = None
                    if parts[2]:
                        build_version = parts[2]
                        build = None

                if not group_slug:
                    print("No group specified.")
                    return []

                if not project_slug:
                    print("No project specified. Aborting.")
                    return []

                if group_slug and not group:
                    group = squad.group(group_slug)
                    if not group:
                        print("Unknown group: {0}".format(group_slug))
                        return []

                if project_slug and not project:
                    project = get_project(squad, group, project_slug)
                    if not project:
                        print("Unknown project: {0}".format(project_slug))
                        return []

                if build_version and not build:
                    build = project.build(build_version)
                    if not build:
                        print("Unknown build: {0}".format(build_version))
                        return []

                # TODO: check if something is missing
                # TODO: add to the list of jobs

    return jobs


def job_to_json(job):
    json = {}

    for triplet_type in ("build", "previous", "base"):
        triplet = job.get(triplet_type)
        if not triplet:
            continue

        json_triplet = {}
        group = triplet.get("group")
        if group:
            json_triplet["group"] = group.slug

        project = triplet.get("project")
        if project:
            json_triplet["project"] = project.slug

        build = triplet.get("build")
        if build:
            json_triplet["build"] = build.version

        json[triplet_type] = json_triplet

    return json
